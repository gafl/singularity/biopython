# biopython Singularity container
### Bionformatics package biopython<br>
Biopython is a set of freely available tools for biological computation written in Python by an international team of developers.<br>
biopython Version: 1.77<br>
[https://biopython.org/]

Singularity container based on the recipe: Singularity.biopython_v1.77

Package installation using Miniconda3-4.7.12<br>

Image singularity (V>=3.3) is automatically build and deployed (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

### build:
`sudo singularity build biopython_v1.77.sif Singularity.biopython_v1.77`

### Get image help
`singularity run-help ./biopython_v1.77.sif`

#### Default runscript: STAR
#### Usage:
  `biopython_v1.77.sif --help`<br>
    or:<br>
  `singularity exec biopython_v1.77.sif python --help`<br>


### Get image (singularity version >=3.3) with ORAS:<br>
`singularity pull biopython_v1.77.sif oras://registry.forgemia.inra.fr/gafl/singularity/biopython/biopython:latest`


